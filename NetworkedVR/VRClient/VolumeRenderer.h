#ifndef VOLUME_RENDERER_H
#define VOLUME_RENDERER_H

#include "Camera.h"
#include "ShaderManager.h"
#include "TransferFunction.h"
#include "Raycaster.h"
#include "ImageProcessor.h"

class VolumeRenderer
{
public:
	Camera camera;
	GLuint shaderProgramID;
	VolumeDataset volume;
	TransferFunction transferFunction;
	Raycaster *raycaster;
	ImageProcessor imageProcessor;
	
	GLuint bruteTex3D, interpTex3D;
	clock_t oldTime;
	int currentTimestep;

	int numFrames;
	float frameTime;

	void Init(int screenWidth, int screenHeight, const std::string transferFunctionFile);
	void Update();
};


#endif